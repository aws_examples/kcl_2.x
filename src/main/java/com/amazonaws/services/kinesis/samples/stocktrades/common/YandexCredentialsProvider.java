package com.amazonaws.services.kinesis.samples.stocktrades.common;

import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;

public class YandexCredentialsProvider implements AwsCredentialsProvider {
    static class YandexCredentials implements AwsCredentials {
        public
        String accessKeyId() {
            return "";
        }

        public
        String secretAccessKey() {
            return "";
        }
    }
    public
    AwsCredentials resolveCredentials() {
        return new YandexCredentials();
    }
}