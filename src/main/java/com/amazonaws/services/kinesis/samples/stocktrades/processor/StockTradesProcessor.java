/*
 * Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/asl/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.amazonaws.services.kinesis.samples.stocktrades.processor;

import java.net.URI;
import java.time.Duration;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.kinesis.samples.stocktrades.common.YandexCredentialsProvider;
import com.amazonaws.services.kinesis.samples.stocktrades.writer.StockTradesWriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.http.Protocol;
import software.amazon.awssdk.http.SdkHttpConfigurationOption;
import software.amazon.awssdk.http.async.SdkAsyncHttpClient;
import software.amazon.awssdk.http.nio.netty.NettyNioAsyncHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.cloudwatch.CloudWatchAsyncClient;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;
import software.amazon.awssdk.utils.AttributeMap;
import software.amazon.kinesis.common.ConfigsBuilder;
import software.amazon.kinesis.common.KinesisClientUtil;
import software.amazon.kinesis.coordinator.Scheduler;
import software.amazon.kinesis.metrics.NullMetricsFactory;

import static software.amazon.awssdk.services.dynamodb.model.BillingMode.PAY_PER_REQUEST;


/**
 * Uses the Kinesis Client Library (KCL) 2.2.9 to continuously consume and process stock trade
 * records from the stock trades stream. KCL monitors the number of shards and creates
 * record processor instances to read and process records from each shard. KCL also
 * load balances shards across all the instances of this processor.
 *
 */
public class StockTradesProcessor {
    private static final Log LOG = LogFactory.getLog(StockTradesProcessor.class);

    private static final Logger ROOT_LOGGER = Logger.getLogger("");
    private static final Logger PROCESSOR_LOGGER =
            Logger.getLogger("com.amazonaws.services.kinesis.samples.stocktrades.processor.StockTradeRecordProcessor");

    private static void checkUsage(String[] args) {
        if (args.length != 3) {
            System.err.println("Usage: " + StockTradesProcessor.class.getSimpleName()
                    + " <application name> <stream name> <region>");
            System.exit(1);
        }
    }

    /**
     * Sets the global log level to WARNING and the log level for this package to INFO,
     * so that we only see INFO messages for this processor. This is just for the purpose
     * of this tutorial, and should not be considered as best practice.
     *
     */
    private static void setLogLevels() {
        ROOT_LOGGER.setLevel(Level.WARNING);
        PROCESSOR_LOGGER.setLevel(Level.WARNING);
    }

    public static void main(String[] args) throws Exception {
        setLogLevels();

        String applicationName = "ydb4yds";
        String streamName = args[0];
        String regionName = args[1];
        URI kinesisEndpoint = new URI(args[2]);
        URI dynamoDbEndpoint = new URI(args[3]);
        Region region = Region.of(regionName);

        System.setProperty(software.amazon.awssdk.core.SdkSystemSetting.CBOR_ENABLED.property(), "false");

        KinesisAsyncClient kinesisClient;
        DynamoDbAsyncClient dynamoClient;
        CloudWatchAsyncClient cloudWatchClient;
        StockTradeRecordProcessorFactory shardRecordProcessor = new StockTradeRecordProcessorFactory();

        YandexCredentialsProvider credentialsProvider = new YandexCredentialsProvider();
        SdkAsyncHttpClient httpClient = NettyNioAsyncHttpClient.builder()
                .protocol(Protocol.HTTP1_1)
                .connectionMaxIdleTime(Duration.ofSeconds(100))
                .build();

        kinesisClient = KinesisAsyncClient.builder()
                .region(region)
                .endpointOverride(kinesisEndpoint)
                .httpClient(httpClient)
                .credentialsProvider(credentialsProvider)
                .build()
        ;
        dynamoClient = DynamoDbAsyncClient.builder()
                .region(region)
                .endpointOverride(dynamoDbEndpoint)
                .credentialsProvider(credentialsProvider)
                .httpClient(httpClient)
                .build()
        ;
        cloudWatchClient = CloudWatchAsyncClient.builder()
                .region(region)
                .credentialsProvider(credentialsProvider)
                .httpClient(httpClient)
                .build()
        ;

        ConfigsBuilder configsBuilder = new ConfigsBuilder(streamName, applicationName,
                kinesisClient,
                dynamoClient,
                cloudWatchClient,
                UUID.randomUUID().toString(),
                shardRecordProcessor
        );

        Scheduler scheduler = new Scheduler(
                configsBuilder.checkpointConfig(),
                configsBuilder.coordinatorConfig(),
                configsBuilder.leaseManagementConfig().billingMode(PAY_PER_REQUEST),
                configsBuilder.lifecycleConfig(),
                configsBuilder.metricsConfig().metricsFactory(new NullMetricsFactory()),
                configsBuilder.processorConfig(),
                configsBuilder.retrievalConfig()
        );
        int exitCode = 0;
        try {
            scheduler.run();
        } catch (Throwable t) {
            LOG.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        System.exit(exitCode);

    }

}
