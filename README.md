# KCL 2.X example
### Credentials
Add your access key and secret key to corresponding methods of `src/main/java/com/amazonaws/services/kinesis/samples/stocktrades/common/YandexCredentialsProvider.java`

### Run your binaries
#### writer
```
$ writer <stream_name> <region_name> <kinesis_endpoint>
$ writer stream ru-central1 https://yds.serverless.yandexcloud.net/ru-central1/the/rest/of/the/endpoint
```

#### processor
```
$ processor <stream_name> <region_name> <kinesis_endpoint> <dynamodb_endpoint>
$ processor stream ru-central1 https://yds.serverless.yandexcloud.net/ru-central1/the/rest/of/the/endpoint https://docapi.serverless.yandexcloud.net/ru-central1/the/rest/of/the/endpoint
```
